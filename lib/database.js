const databaseConfig = {
    user: '<YOUR_USER>',
    host: '127.0.0.1',
    password: '<YOUR_SECRET>',
    database: 'lims_cn_sw',
    port: 5432
};


module.exports = databaseConfig;